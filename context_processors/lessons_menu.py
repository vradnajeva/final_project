from django.template.context_processors import request

from lessons.models import Lesson
from tests.models import LessonTest


def lessons_menu(request):
    lessons_list = Lesson.objects.all()

    return {'lessons_list': lessons_list}


def tests_menu(request):
    tests_list = LessonTest.objects.all()

    return {'tests_list': tests_list}
