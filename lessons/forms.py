from django import forms
from ckeditor.widgets import CKEditorWidget
from . import models


class LessonEditForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = models.Lesson
        fields = ['title', 'short_text', 'text']


class LessonCreateForm(forms.ModelForm):
    text = forms.CharField(widget=CKEditorWidget())

    class Meta:
        model = models.Lesson
        fields = '__all__'
