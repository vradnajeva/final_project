from django.urls import path, include
from django.views.generic import DeleteView

from . import views

urlpatterns = [
    path('', views.lessons_list, name='all_lessons'),
    path('list/', views.lessons_list, name='list'),
    path('list/<int:pk>/', views.lessons_detail, name='detail'),
    path('list/create/', views.lessons_create, name='create'),
    path('list/<int:pk>/update/', views.lessons_update, name='update'),
    path('list/<int:pk>/delete/', views.lessons_delete, name='delete'),
    path('list/<int:pk>/tests/', include(('tests.urls', 'tests')), name='tests')

]
