from django.shortcuts import render, get_object_or_404, redirect
from tests.models import LessonTest

from .forms import LessonEditForm, LessonCreateForm
from .models import Lesson


def lessons_list(req):
    lesson = Lesson.objects.filter(is_published=True)
    return render(
        req,
        'lessons/list.html',
        {
            'lesson': lesson
        }
    )


def lessons_detail(request, pk):
    lesson = get_object_or_404(Lesson, pk=pk)
    try:
        test = LessonTest.objects.get(lesson=lesson.id, pk=pk)
    except Exception:
        test = None

    return render(
        request,
        'lessons/detail.html',
        {
            'lesson': lesson,
            'test': test
        }
    )


def lessons_update(request, pk):
    lesson = get_object_or_404(Lesson, pk=pk)
    if request.method == 'GET':
        form = LessonEditForm(instance=lesson)
        return render(
            request,
            'lessons/update.html',
            {
                'lesson': lesson,
                'form': form
            }
        )
    if request.method == 'POST':
        form = LessonEditForm(request.POST, instance=lesson)
        if form.is_valid():
            form.save()
            return redirect('lessons:detail', pk=lesson.pk)
    return render(request, 'lessons/update.html', {'lesson': lesson})


def lessons_delete(request, pk):
    if request.user.is_superuser:
        lesson = get_object_or_404(Lesson, pk=pk)
        lesson.delete()
    return redirect('lessons:list')


def lessons_create(request):
    if request.method == 'GET':
        return render(
            request,
            'lessons/create.html',
            {
                'form': LessonCreateForm()
            }
        )
    if request.method == 'GET':
        form = LessonCreateForm()
        return render(
            request,
            'lessons/create.html',
            {
                'form': form
            }
        )
    if request.method == 'POST':
        form = LessonEditForm(request.POST)
        if form.is_valid():
            lesson = Lesson.objects.create(
                title=form.cleaned_data['title'],
                short_text=form.cleaned_data['short_text'],
                text=form.cleaned_data['text'],
                is_published=True,
            )
            lesson.save()
            return redirect('lessons:list')
        else:
            return render(request, 'lessons/create.html', {'form': form})
