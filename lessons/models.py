from django.db import models
from ckeditor.fields import RichTextField


class Lesson(models.Model):
    #project_code = RichTextField(verbose_name='Code', null=True, blank=True)

    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255,
    )
    short_text = models.TextField(
        verbose_name='Короткий текст',
        max_length=140,
        null=True
    )
    text = RichTextField(
        verbose_name='Текст урока',
        default='..................',
        # editable=False
    )
    is_published = models.BooleanField(
        default=False
    )

    def __str__(self):
        return f'{self.title}'#{self.id}
