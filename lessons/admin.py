from django.contrib import admin

from .models import Lesson


def publish(self, request, queryset):
    queryset.update(is_published=True)


class LessonAdmin(admin.ModelAdmin):
    list_display = [
        'id',
        'title',
        'is_published'
    ]
    list_filter = [
        'is_published',
    ]
    actions = [
        # 'publish'
        publish,
        'unpublish'
    ]

    def unpublish(self, request, queryset):
        queryset.update(is_published=False)

admin.site.register(Lesson, LessonAdmin)
