# Generated by Django 2.1.1 on 2018-09-03 19:02

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('lessons', '0001_initial'),
        ('tests', '0002_auto_20180901_0828'),
    ]

    operations = [
        migrations.CreateModel(
            name='LessonTest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lesson_id', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='lessons.Lesson')),
            ],
        ),
        migrations.CreateModel(
            name='PossibleAnswer',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('possible_answer1', models.CharField(max_length=100)),
                ('possible_answer2', models.CharField(max_length=100)),
                ('possible_answer3', models.CharField(max_length=100)),
                ('possible_answer4', models.CharField(max_length=100)),
            ],
        ),
        migrations.RemoveField(
            model_name='question',
            name='answer_2',
        ),
        migrations.RemoveField(
            model_name='question',
            name='answer_3',
        ),
        migrations.RemoveField(
            model_name='question',
            name='answer_4',
        ),
        migrations.RemoveField(
            model_name='question',
            name='ok_answer',
        ),
        migrations.RemoveField(
            model_name='question',
            name='question',
        ),
        migrations.RemoveField(
            model_name='resultdata',
            name='success',
        ),
        migrations.AddField(
            model_name='question',
            name='question_text',
            field=models.CharField(default='?', max_length=255, unique=True, verbose_name='Вопрос'),
        ),
        migrations.AlterField(
            model_name='resultdata',
            name='result',
            field=models.PositiveIntegerField(default=0, null=True),
        ),
        migrations.AddField(
            model_name='possibleanswer',
            name='question_id',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='tests.Question'),
        ),
        migrations.AddField(
            model_name='question',
            name='test_id',
            field=models.ForeignKey(default=0, on_delete=django.db.models.deletion.CASCADE, to='tests.LessonTest'),
        ),
    ]
