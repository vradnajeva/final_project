# Generated by Django 2.1.1 on 2018-09-07 22:47

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('tests', '0017_auto_20180907_1101'),
    ]

    operations = [
        migrations.AddField(
            model_name='lessontest',
            name='short_text',
            field=models.TextField(max_length=140, null=True, verbose_name='Короткий текст'),
        ),
        migrations.AddField(
            model_name='lessontest',
            name='title',
            field=models.CharField(default='Тест', max_length=255, verbose_name='Заголовок'),
        ),
        migrations.AlterField(
            model_name='question',
            name='test',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='questions', to='tests.LessonTest'),
        ),
        migrations.AlterField(
            model_name='questionchoice',
            name='question',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='choices', to='tests.Question'),
        ),
    ]
