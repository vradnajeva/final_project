from django.contrib.auth.models import User
from django.db import models
from lessons import models as lesson_models


class LessonTest(models.Model):
    title = models.CharField(
        verbose_name='Заголовок',
        max_length=255,
        default='Тест'
    )
    short_text = models.TextField(
        verbose_name='Короткий текст',
        max_length=140,
        null=True
    )

    lesson = models.OneToOneField(
        lesson_models.Lesson,
        related_name='test',
        on_delete=models.CASCADE
    )

    def __str__(self):
        return f'{self.lesson_id}'


class Question(models.Model):
    question_text = models.CharField(
        max_length=255,
        unique=True,
        verbose_name='Вопрос', default='?'
    )
    correct_answer = models.CharField(
        verbose_name='Ответ',
        max_length=100)
    test = models.ForeignKey(
        LessonTest,
        on_delete=models.CASCADE,
        related_name='questions'
    )

    def __str__(self):
        return f'{self.question_text}'


class QuestionChoice(models.Model):
    question = models.ForeignKey(
        Question, on_delete=models.CASCADE,
        related_name='choices'
    )
    text = models.CharField(
        max_length=80,
        verbose_name='Текст'
    )

    def __str__(self):
        return f'{self.text}'


class ResultData(models.Model):
    test = models.ForeignKey(LessonTest, verbose_name="Тест", on_delete=models.CASCADE)
    name_user = models.ForeignKey(User, on_delete=models.CASCADE)
    ok_answer = models.CharField(max_length=100, null=True)
    wrong_answer = models.CharField(max_length=100, null=True)
    result = models.PositiveIntegerField(null=True, default=0)

    def __str__(self):
        return f'{self.name_user}'
