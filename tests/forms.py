from django.contrib.auth.models import User
from django import forms
from django.forms import HiddenInput, formset_factory, Widget

from .models import ResultData, LessonTest, Question, QuestionChoice
from lessons.models import Lesson


class QuestionCreateForm(forms.Form):
    question_text = forms.CharField(
        label='Вопрос',
        widget=forms.TextInput(attrs={'placeholder': 'Вопрос'})
    )
    correct_answer = forms.CharField(
        label='Правильный ответ',
        widget=forms.TextInput(attrs={'placeholder': 'Правильный ответ'})
    )
    choice_1 = forms.CharField(
        label='Вариант ответа 1',
        widget=forms.TextInput(attrs={'placeholder': 'Вариант ответа 1'})
    )
    choice_2 = forms.CharField(
        label='Вариант ответа 2',
        widget=forms.TextInput(attrs={'placeholder': 'Вариант ответа 2'})
    )
    choice_3 = forms.CharField(
        label='Вариант ответа 3',
        widget=forms.TextInput(attrs={'placeholder': 'Вариант ответа 3'})
    )
    choice_4 = forms.CharField(
        label='Вариант ответа 4',
        widget=forms.TextInput(attrs={'placeholder': 'Вариант ответа 4'})
    )
    test = forms.ModelChoiceField(
        label='Тест',
        queryset=LessonTest.objects.all()
    )

    def clean(self):
        cleaned_data = super().clean()
        choices = []
        for n in range(1, 5):
            choices.append(cleaned_data[f'choice_{n}'])
        for m in range(1, 5):
            correct_answer = cleaned_data['correct_answer']
            choice_m = cleaned_data[f'choice_{m}']
            if choices.count(choice_m) > 1:
                raise forms.ValidationError('Варианты ответа не должны повторяться!')
            elif correct_answer not in choices:
                raise forms.ValidationError('В вариантах ответа нет правильного!')

        return cleaned_data


class TestCreateForm(forms.Form):
    lesson = forms.ModelChoiceField(
        label='Урок',
        queryset=Lesson.objects.filter(test__isnull=True, is_published=True)
    )
    question_text = forms.CharField(
        label='Текст вопроса',
        widget=forms.TextInput(attrs={'placeholder': 'Текст вопроса'})
    )
    correct_answer = forms.CharField(
        label='Правильный ответ',
        widget=forms.TextInput(attrs={'placeholder': 'Правильный ответ'})
    )
    choice_1 = forms.CharField(
        label='Вариант ответа 1',
        widget=forms.TextInput(attrs={'placeholder': 'Вариант ответа 1'})
    )
    choice_2 = forms.CharField(
        label='Вариант ответа 2',
        widget=forms.TextInput(attrs={'placeholder': 'Вариант ответа 2'})
    )
    choice_3 = forms.CharField(
        label='Вариант ответа 3',
        widget=forms.TextInput(attrs={'placeholder': 'Вариант ответа 3'})
    )
    choice_4 = forms.CharField(
        label='Вариант ответа 4',
        widget=forms.TextInput(attrs={'placeholder': 'Вариант ответа 4'})
    )


class SpanWidget(Widget):
    template_name = 'widgets/span.html'


class QuestionForm(forms.Form):
    question_id = forms.CharField(widget=HiddenInput, disabled=True)
    question_text = forms.CharField(label='Вопрос', widget=SpanWidget, disabled=True)
    answer = forms.ModelChoiceField(
        label='Ответ',
        queryset=QuestionChoice.objects.all(),
        widget=forms.RadioSelect,
        empty_label=None,
        error_messages={'required': 'Вы не ответили на этот вопрос.'}
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if kwargs.get("initial"):
            self.fields['answer'].queryset = QuestionChoice.objects.filter(question_id=kwargs["initial"]["question_id"])

    def clean_answer(self):
        data = self.cleaned_data['answer']
        if not data:
            raise forms.ValidationError("Вы не ответили на этот вопрос.")
        return data


BaseTestFormSet = formset_factory(QuestionForm)


class TestFormSet(BaseTestFormSet):
    extra = 0

    def __init__(self, *args, **kwargs):
        super(TestFormSet, self).__init__(*args, **kwargs)

    def _construct_form(self, *args, **kwargs):
        return super(TestFormSet, self)._construct_form(*args, **kwargs)
