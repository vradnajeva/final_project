from django.db import transaction
from django.forms import formset_factory
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse

from tests.forms import QuestionForm
from .models import LessonTest, Question, ResultData
from . import forms
from . import models
from lessons.models import Lesson


def test_create_view(request, pk):
    if request.method == 'GET':
        form = forms.TestCreateForm()
        return render(
            request,
            template_name='create.html',
            context={
                'form': form
            }
        )
    if request.method == 'POST':
        form = forms.TestCreateForm(request.POST, request.FILES)
        if form.is_valid():
            with transaction.atomic():
                lesson_test = models.LessonTest.objects.create(
                    lesson=form.cleaned_data['lesson']
                )
                question = models.Question.objects.create(
                    test=lesson_test,
                    question_text=form.cleaned_data['question_text'],
                    correct_answer=form.cleaned_data['correct_answer'],
                )
                models.QuestionChoice.objects.create(
                    question=question,
                    text=form.cleaned_data['choice_1'],
                )
                models.QuestionChoice.objects.create(
                    question=question,
                    text=form.cleaned_data['choice_2'],
                )
                models.QuestionChoice.objects.create(
                    question=question,
                    text=form.cleaned_data['choice_3'],
                )
                models.QuestionChoice.objects.create(
                    question=question,
                    text=form.cleaned_data['choice_4'],
                )

            return redirect('/')
        else:
            return render(
                request,
                template_name='create.html',
                context={
                    'form': form
                }
            )


def question_create_view(request, pk):
    if request.method == 'GET':
        form = forms.QuestionCreateForm()
        return render(
            request,
            template_name='add_question.html',
            context={
                'form': form
            }
        )
    if request.method == 'POST':
        form = forms.QuestionCreateForm(request.POST)
        lesson = Lesson.objects.get(pk=pk)
        if form.is_valid():
            with transaction.atomic():
                question = models.Question.objects.create(
                    test=lesson.test,
                    question_text=form.cleaned_data['question_text'],
                    correct_answer=form.cleaned_data['correct_answer'],
                )
                models.QuestionChoice.objects.create(
                    question=question,
                    text=form.cleaned_data['choice_1'],
                )
                models.QuestionChoice.objects.create(
                    question=question,
                    text=form.cleaned_data['choice_2'],
                )
                models.QuestionChoice.objects.create(
                    question=question,
                    text=form.cleaned_data['choice_3'],
                )
                models.QuestionChoice.objects.create(
                    question=question,
                    text=form.cleaned_data['choice_4'],
                )

            return redirect('/')
        else:
            return render(
                request,
                template_name='add_question.html',
                context={
                    'form': form
                }
            )


def tests_detail(request, pk, test_id):
    data = request.POST or None

    questions = Question.objects.filter(test_id=test_id)
    initial_data = [{'question_id': p.id, 'question_text': p.question_text} for p in questions]
    formset = forms.TestFormSet(data=data, initial=initial_data)

    if request.method == 'GET':
        return render(
            request,
            template_name='tests/detail.html',
            context={
                'test_form_set': formset
            }
        )
    if request.method == 'POST':
        if formset.is_valid():
            questions = Question.objects.filter(test_id=test_id)
            test = LessonTest.objects.get(id=test_id)

            count = questions.count()
            ok_answer = 0

            for quest in formset.cleaned_data:
                quest_obj = Question.objects.get(id=quest.get("question_id"))

                if quest_obj.correct_answer == quest.get("answer").text:
                    ok_answer += 1

            wrong_answer = count - ok_answer
            result = int(ok_answer / count * 100)
            result_data = ResultData(
                test=test,
                name_user=request.user,
                ok_answer=ok_answer,
                wrong_answer=wrong_answer,
                result=result
            )
            result_data.save()

            return redirect(reverse("lessons:tests:result", args=[pk, result_data.id]))
        else:
            return render(
                request,
                template_name='tests/detail.html',
                context={
                    'test_form_set': formset
                }
            )


def result_detail(request, pk, result_id):
    result_data = get_object_or_404(ResultData, pk=result_id)

    return render(
        request,
        template_name='result_data/detail.html',
        context={
            'result_data': result_data
        }
    )
