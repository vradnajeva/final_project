from django.contrib import admin

from .models import Question, LessonTest, QuestionChoice


admin.site.register(Question)
admin.site.register(QuestionChoice)
admin.site.register(LessonTest)
