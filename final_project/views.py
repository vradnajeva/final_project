from django.contrib import auth
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic


def index(request):
    username = auth.get_user(request).username
    return render(request, 'layout.html',
                  {
                      'username': username
                  })


class SignUp(generic.CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('login')
    template_name = 'signup.html'


def home(request):
    username = auth.get_user(request).username
    return render(request, 'home.html', {
        'username': username
    })


def login(request):
    context = {}
    if request.method == 'POST':
        username = request.POST.get('username', '')
        password = request.POST.get('password', '')
        user = auth.authenticate(username=username, password=password)
        if user is not None:
            auth.login(request, user)
            return redirect('/')
        else:
            login_error = 'Пользователь не существует'
            context = {'login_error': login_error}
            return render(request, 'registration/login.html', context)
    else:
        return render(request, 'registration/login.html', context)


def logout(request):
    auth.logout(request)
    return redirect('/')
