from django.shortcuts import render, redirect
from .forms import ProfileForm
from django.contrib.auth.decorators import login_required
from django.db import transaction


@login_required
@transaction.atomic
def update_profile(request):
    if request.method == 'POST':
        profile_form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if profile_form.is_valid():
            profile_form.save()
            return redirect('/profile')
        else:
            return render(
                request,
                'profiles/update.html',
                {
                    'profile_form': profile_form
                }
            )
    else:
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'profiles/update.html', {
        'profile_form': profile_form
    })


def show_profile(request):
    prof = request.user.profile
    return render(request, 'profiles/profile.html',
                  {
                      'profile': prof,
                  })
