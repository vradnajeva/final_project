from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    avatar = models.ImageField(upload_to='user_profiles/avatars', null=True, blank=True)
    bio = models.TextField(max_length=500, blank=True, verbose_name='Биография')
    first_name = models.CharField(max_length=50, blank=True, verbose_name='Имя')
    last_name = models.CharField(max_length=50, blank=True, verbose_name='Фамилия')
    email = models.CharField(max_length=50, blank=True, verbose_name='Почта')
    location = models.CharField(max_length=50, blank=True, verbose_name='Местоположение')

    def __str__(self):
        return f' {self.bio} {self.first_name} {self.last_name} {self.email} {self.location}'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
